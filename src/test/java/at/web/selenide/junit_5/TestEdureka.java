package at.web.selenide.junit_5;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestEdureka {
    @BeforeAll
    public static void startBrowser(){
        System.out.println("startBrowser");
        Edureka.startBrowser();
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: Compare Titles")
    public void checkTitleEdureka() {
        Edureka.checkTitleEdureka();
    }

    @AfterAll
    public static void closeBrowser(){
        Edureka.closeBrowser();
        System.out.println("closeBrowser");

    }
}
