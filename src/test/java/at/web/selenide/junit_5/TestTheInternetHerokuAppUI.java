package at.web.selenide.junit_5;

import org.junit.jupiter.api.*;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestTheInternetHerokuAppUI {
    @BeforeAll
    public static void startBrowser(){
        System.out.println("startBrowser");
        TheInternetHerokuAppUI.startBrowser();
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: checkBox")
    public void checkBox() {
        TheInternetHerokuAppUI.checkBox();

    }

    @Order(2)
    @Test
    @DisplayName("Cценарий: dropDownMenu")
    public void dropDownMenu() {
        TheInternetHerokuAppUI.dropDownMenu();

    }

    @Order(3)
    @Test
    @DisplayName("Cценарий: drugAndDropJavaScript")
    public void drugAndDropJavaScript() throws IOException {
        TheInternetHerokuAppUI.drugAndDropJavaScript();

    }

    @AfterAll
    public static void closeBrowser(){
        TheInternetHerokuAppUI.closeBrowser();
        System.out.println("closeBrowser");

    }
}
