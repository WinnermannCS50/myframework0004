package at3.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

/**
 * PageObject
 * Страница CheckBoxPage с элементами
 *
 */
public class CheckBoxPage {

    //Поле Checkbox1
    private SelenideElement checkbox1OnCheckBoxPage = element(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)"));
    //Поле Checkbox2
    private SelenideElement checkbox2OnCheckBoxPage = element(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)"));


    /**
     * Действия с полем Checkbox1
     */
//    //Проверяет что Чек-бокс не выбран
//    public void makeSureCheckBox1IsNotSelected(){
//        this.checkbox1OnCheckBoxPage.shouldNotBe(checked);
//    }
//
//    //Проставляет Чек-бокс
//    public void setCheckbox1Selected(){
//        this.checkbox1OnCheckBoxPage.setSelected(true);
//    }
//
//    //Проверяет что Чек-бокс выбран
//    public void makeSureCheckBox1IsSelected(){
//        this.checkbox1OnCheckBoxPage.shouldBe(checked);
//    }
//
//    /**
//     * Действия с полем Checkbox2
//     */
//    //Проверяет что Чек-бокс не выбран
//    public void makeSureCheckBox2IsNotSelected(){
//        this.checkbox2OnCheckBoxPage.shouldNotBe(checked);
//    }
//
//    //Проставляет Чек-бокс
//    public void setCheckbox2Unselected(){
//        this.checkbox2OnCheckBoxPage.setSelected(false);
//    }
//
//    //Проверяет что Чек-бокс выбран
//    public void makeSureCheckBox2IsSelected(){
//        this.checkbox2OnCheckBoxPage.shouldBe(checked);
//    }


    public SelenideElement getCheckbox1OnCheckBoxPage() {
        return checkbox1OnCheckBoxPage;
    }

    public void setCheckbox1OnCheckBoxPage(SelenideElement checkbox1OnCheckBoxPage) {
        this.checkbox1OnCheckBoxPage = checkbox1OnCheckBoxPage;
    }

    public SelenideElement getCheckbox2OnCheckBoxPage() {
        return checkbox2OnCheckBoxPage;
    }

    public void setCheckbox2OnCheckBoxPage(SelenideElement checkbox2OnCheckBoxPage) {
        this.checkbox2OnCheckBoxPage = checkbox2OnCheckBoxPage;
    }
}
