package at3.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * PageObject
 * Страница DropdownMenuPage с элементами
 */
public class DropdownMenuPage {

    //Выпадающее меню (родительское)
    private SelenideElement parentDiv = $("#dropdown");
    //Выпадающее меню (option1)
    private SelenideElement option1 = $("option:nth-child(2)");
    //Выпадающее меню (option2)
    private SelenideElement option2 = $("option:nth-child(3)");



    public SelenideElement getParentDiv(String s) {
        return parentDiv;
    }

    public void setParentDiv(SelenideElement parentDiv) {
        this.parentDiv = parentDiv;
    }

    public SelenideElement getOption1() {
        return option1;
    }

    public void setOption1(SelenideElement option1) {
        this.option1 = option1;
    }

    public SelenideElement getOption2() {
        return option2;
    }

    public void setOption2(SelenideElement option2) {
        this.option2 = option2;
    }


}
