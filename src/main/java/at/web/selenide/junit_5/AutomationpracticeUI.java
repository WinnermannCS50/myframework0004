package at.web.selenide.junit_5;

import at.web.selenide.config.configAutomationpracticeUI.UserConfig;
import at.web.selenide.pages.AutomationpracticeUI.SignInPage;
import at.web.selenide.pages.AutomationpracticeUI.StartPage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AutomationpracticeUI {
    static SignInPage signInPage = new SignInPage();
    static StartPage startPage = new StartPage();

    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open(UserConfig.URL);
        //Выводит  заголовок страницы в консоль
        System.out.println(Selenide.title());
        //Проверяет, что заголовок страницы правильный
        assertTrue(Selenide.title().equals("My Store"));

    }

    public static void login(){
        //Проврить имя кнопки
        //$("#header div.header_user_info a").shouldHave(Condition.text("Sign in")).click();
        startPage.checkSignInButtonName("Sign in");

        //Нажать на кнопку "Sign in"
        startPage.clickSignInButton();

        //Заполняет поле email невалидным адресом
        //$("#email").setValue("admin");
        signInPage.inputLogin(UserConfig.USER_LOGIN);

        //Заполняет поле пароль и нажимает клавишу Enter
        //$("#passwd").setValue("123456").pressEnter();
        signInPage.inputPassword(UserConfig.USER_PASSWORD);

        //Кликнуть кнопку "Sign In"
        signInPage.clickSignInButton();

        //Проверяет, что отобразилось сообщение "Invalid email address."
        //$(By.xpath("//li[contains(text(),'Invalid email address.')]")).shouldHave(Condition.text("Invalid email address."));
        signInPage.shouldDisplayErrorMessage("Invalid email address.");

    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
