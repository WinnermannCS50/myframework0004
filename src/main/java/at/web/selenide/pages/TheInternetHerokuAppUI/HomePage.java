package at.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

/**
 * PageObject
 * Страница HomePage с элементами
 * textWelcomeOnHomePage - Слова "Welcome to the-internet" на странице "HomePage"
 */
public class HomePage {
    //Слова "Welcome to the-internet" на странице "HomePage"
    private SelenideElement textWelcomeOnHomePage = element(By.cssSelector("#content h1"));
    //Слова "Checkboxes" на странице "HomePage"
    private SelenideElement textCheckboxesOnHomePage = element(By.cssSelector("#content li:nth-child(6) a"));
    //Слова "Dropdown" на странице "HomePage"
    private SelenideElement textDropdownOnHomePage = element(By.cssSelector("#content li:nth-child(11) a"));
    //Слова "DragAndDrop" на странице "HomePage"
    private SelenideElement textDragAndDropOnHomePage = element(By.cssSelector("#content li:nth-child(10) a"));

    //Убедиться что на странице есть слова "Welcome to the-internet"
    public void shouldCheckWelcomeTextOnHomePage(String text){
        this.textWelcomeOnHomePage.shouldHave(Condition.text(text));
    }

    //Убедиться что ссылка содержит слова "Checkboxes" и перейти по ссылке
    public void shouldCheckCheckboxesTextOnHomePage(String text){
        this.textCheckboxesOnHomePage.shouldHave(Condition.text(text)).click();
    }

    //Убедиться что ссылка содержит слова "Dropdown" и перейти по ссылке
    public void shouldCheckDropdownTextOnHomePage(String text){
        this.textDropdownOnHomePage.shouldHave(Condition.text(text)).click();
    }

    public void shouldCheckDragAndDropTextOnHomePage(String text){
        this.textDragAndDropOnHomePage.shouldHave(Condition.text(text)).click();
    }
}
