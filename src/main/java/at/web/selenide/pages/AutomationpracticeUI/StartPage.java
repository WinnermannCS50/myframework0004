package at.web.selenide.pages.AutomationpracticeUI;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * PageObject
 * Страница StartPage с элементами
 */
public class StartPage {
    /**
     * Локаторы элементов на странице
     */
    private SelenideElement signInButton = $("#header div.header_user_info a");

    /**
     * Нажатие на кнопку signInButton
     */
    public void clickSignInButton(){
        signInButton.click();
    }

    /**
     * Проверка имени кнопки
     * @param text - переменная содержит имя кнопки
     */
    public void checkSignInButtonName(String text){
        this.signInButton.shouldHave(Condition.text(text));
    }
}
