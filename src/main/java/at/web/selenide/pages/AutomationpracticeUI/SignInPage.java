package at.web.selenide.pages.AutomationpracticeUI;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * PageObject
 * Страница SignInPage с элементами
 * loginInput - поле ввода пароля
 * passwordInput - поле ввода логина
 * signInButton - кнопка авторизации в личный кабинет
 * errorMessage - сообщение об ошибке логина с невалидными данными
 */
public class SignInPage {

    /**
     * Локаторы элементов на странице
     */
    private SelenideElement loginInput = $("#email");
    private SelenideElement passwordInput = $("#passwd");
    private SelenideElement signInButton = $("#SubmitLogin");
    private SelenideElement errorMessage = $(By.xpath("//li[contains(text(),'Invalid email address.')]"));

    /**
     * Ввод текста в поле loginInput
     * @param text - переменная содержит логин
     */
    public void inputLogin(String text){
        this.loginInput.setValue(text);
    }

    /**
     * Ввод текста в поле passwordInput
     * @param text - переменная содержит пароль
     */
    public void inputPassword(String text){
        this.passwordInput.val(text);
    }

    /**
     * Нажатие на кнопку signInButton
     */
    public void clickSignInButton(){
        //$(text).click();
        signInButton.click();
        //this.signInButton();
    }

    /**
     * Проверка текста в сообщении
     * @param text
     */
    public void shouldDisplayErrorMessage(String text){
        this.errorMessage.shouldHave(Condition.text(text));
    }

}
