package at2.web.selenide.junit_5;

import at2.web.selenide.pages.TheInternetHerokuAppUI.CheckBoxPage;
import at2.web.selenide.pages.TheInternetHerokuAppUI.DropdownMenuPage;
import at2.web.selenide.pages.TheInternetHerokuAppUI.DrugAndDropJavaScriptPage;
import at2.web.selenide.pages.TheInternetHerokuAppUI.HomePage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TheInternetHerokuAppUI {
    static HomePage homePage = new HomePage();
    static CheckBoxPage checkBoxPage = new CheckBoxPage();
    static DropdownMenuPage dropdownMenuPage = new DropdownMenuPage();
    static DrugAndDropJavaScriptPage drugAndDropJavaScriptPage = new DrugAndDropJavaScriptPage();
    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("http://the-internet.herokuapp.com");
        //Выводит  заголовок страницы в консоль
        System.out.println(Selenide.title());
        //Проверяет, что заголовок страницы правильный
        assertTrue(Selenide.title().equals("The Internet"));

    }

    public static void checkBox(){
        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextCheckboxesOnHomePage().getOwnText(), "Checkboxes");
        //перейти по ссылке "Checkboxes"
        homePage.getTextCheckboxesOnHomePage().click();

        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldNotBe(checked);
        //Проставляет Чек-бокс
        checkBoxPage.getCheckbox1OnCheckBoxPage().setSelected(true);
        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldBe(checked);

        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldBe(checked);
        //Снимает Чек-бокс
        checkBoxPage.getCheckbox2OnCheckBoxPage().setSelected(false);
        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldNotBe(checked);

    }


    public static void dropDownMenu(){
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextDropdownOnHomePage().getOwnText(), "Dropdown");
        //перейти по ссылке "Checkboxes"
        homePage.getTextDropdownOnHomePage().click();


        //Выбрать из выпадающего меню Option 2
        //dropdownMenuPage.shouldSelectOption2();
        dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").scrollTo().click();
        //проверяет что меню Option 2 выбрано
        //dropdownMenuPage.shouldCheckOption2IsSelected();
        dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").shouldHave(text("Option 2"));


        //Выбрать из выпадающего меню Option 1
        //parentDiv.find("option:nth-child(2)").scrollTo().click();
        //dropdownMenuPage.shouldSelectOption1();
        dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").scrollTo().click();
        //проверяет что меню Option 1 выбрано
        //parentDiv.find("option:nth-child(2)").shouldHave(text("Option 1"));
        //dropdownMenuPage.shouldCheckOption1IsSelected();
        dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").shouldHave(text("Option 1"));
    }

    public static void drugAndDropJavaScript() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Drag and Drop"
        assertEquals(homePage.getTextDragAndDropOnHomePage().getOwnText(), "Drag and Drop");
        //перейти по ссылке "Drag and Drop"
        homePage.getTextDragAndDropOnHomePage().click();


        //Проверяет что элемент КолонкаА содержит текст А
        //element(By.cssSelector("#column-a")).shouldHave(text("A"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("A");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("A"));
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "A");

        //Создадим объект класса File
        File dnd_javascript = new File("scripts/dnd.js");
        FileReader reader = new FileReader(dnd_javascript);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        bufferedReader.lines().forEach(
                o->builder.append(o).append('\n')
        );

        String javaScript = builder.toString();
        javaScript = javaScript + " simulateDragDrop(document.getElementById('column-a'),document.getElementById('column-b'));";
        Selenide.executeJavaScript(javaScript);

        //Проверяет что элемент КолонкаА содержит текст Б
        //element(By.cssSelector("#column-a")).shouldHave(text("B"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("B");
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "B");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("B"));

    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
